#!/usr/bin/env python

import player

class passive_player(player.player):
	moves = None
	position = 0

	def __init__(self, color, moves, skip=False):
		super(passive_player,self).__init__(color)

		self.moves = moves
		if skip: # for passive white player
			self.position = 1

	def make_move(self, game_state):
		move = self.moves[self.position].strip()
		self.position += 2
		return move

	def peek_move(self, game_state):
		return self.moves[self.position].strip()

	# In Go, winning the game by 30 points DOES NOT mean you are way better than
	# that person in comparison to winning by 1 point.  All we need to know is
	# who won to judge who played better.
	def results(self, game_state):
		winner = self.moves[len(self.moves)-1].split(":")[1][0]
		return winner
