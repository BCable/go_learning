#!/usr/bin/env python

import re, subprocess

import player

class gnugo_player(player.player):
	next_move = None
	proc = None
	size = None

	def __init__(self, color, size, komi):
		super(gnugo_player,self).__init__(color)
		self.size = size

		# players color, not GNUGo's color
		if self.color == "B":
			color = "white"
		else:
			color = "black"

		self.proc = subprocess.Popen(
			["gnugo",
				"--mode", "ascii", "--boardsize", str(size),
				"--komi", str(komi), "--color", color
			],
			stdin=subprocess.PIPE,
			stdout=subprocess.PIPE, stderr=subprocess.PIPE
		)

		# toggles the board off, since we aren't watching it
		#self.send_command("showboard")

	def send_command(self, cmd):
		self.proc.stdin.write("%s\n" % cmd)

	def get_move(self, move=None):
		# make the move
		if move != None:
			# flush output so this move doesn't get detected as the opponent's
			# move
			move = move.lower()
			self.proc.stdout.flush()
			self.proc.stdin.write("%s\n" % move)

		# figure out the response
		match = False
		while not match:
			newline = self.proc.stdout.readline().strip("\r\n")
			newline = newline.strip()

			if self.color == "B":
				color = "black"
			else:
				color = "white"

			# this regexp is in generator.py as well (in case this gets changed)
			move = re.sub(
				"^%s\([0-9]+\):(?: ([A-HJ-T][0-9]+|PASS))$" % color,
				"\\1", newline
			)
			if move != newline:
				match = True

		# return response
		return move.strip()

	def make_move(self, game_state):
		if self.next_move == None:
			move = self.peek_move(game_state)
		else:
			move = self.next_move

		self.next_move = None
		return move

	def peek_move(self, game_state):
		if self.next_move == None:
			# get move from gnugo when first move
			if game_state.this_move == None and game_state.last_move == None:
				self.next_move = self.color + ":" + self.get_move()

			# get move from gnugo when not first move
			else:
				last_move = game_state.this_move

				# someone made a pass move somewhere
				if last_move == None:
					self.next_move = self.color + ":PASS"

				# real move
				else:
					self.next_move = self.color + ":" + self.get_move(
						game_state.column_encode(last_move[0]) + \
						str(game_state.row_encode(last_move[1]))
					)

		return self.next_move

	def results(self, game_state):
		at_end = False
		score = False

		if (
			game_state != None and
			game_state.last_raw_move != None and
			game_state.this_raw_move != None
		):
			if (
				game_state.last_raw_move[-5:] == ":PASS" and
				game_state.this_raw_move[-5:] == ":PASS"
			):
				at_end = True

			# retrieve results and close GNUGo process
			if at_end:
				# just in case the pass didn't get through
				self.send_command("pass")

				# get results
				line = self.proc.stdout.readline()
				while line != None:
					if line[0:8] == "Result: ":
						score = line[8:]
						break
					line = self.proc.stdout.readline()

				# quit
				self.send_command("quit")
				self.proc.wait()

		return score
