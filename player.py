#!/usr/bin/env python

class player(object):
	color = None

	def __init__(self, color):
		self.color = color

	def make_move(self, game_state):
		pass

	def peek_move(self, game_state):
		pass

	def results(self, game_state):
		pass
