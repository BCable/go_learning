#!/usr/bin/env python

from board_chunk import board_chunk
from tdlearner import tdlearner

# each position is "B" for black, "W" for white, or "." for empty

class game_state(object):
	size = 9
	komi = 5.5
	chunk_size_relative = 3
	chunk_size_absolute = 3
	board = None
	winner = None

	# last/this stuff
	last_raw_move = None
	last_move = None
	last_player = "W"
	last_reward = 0
	this_raw_move = None
	this_move = None
	this_player = "B"
	this_reward = 0

	# tdlearner
	tdlearner = None

	def __init__(self, winner, tdlearner=None):
		self.winner = winner

		self.board = []
		for i in xrange(0,self.size):
			self.board.append(["."]*self.size)

		self.tdlearner = tdlearner

	def set_pos(self, color, posX, posY):
		self.board[posY][posX] = color

	def get_pos(self, posX, posY):
		return self.board[posY][posX]

	def column_decode(self, letter):
		num = ord(letter)-65
		if num > 7: # GNUGo skips "I"
			num -= 1
		return num

	def column_encode(self, num):
		if num > 7: # GNUGo skips "I"
			num += 1
		letter = chr(65+num)
		return letter

	def row_decode(self, num):
		return self.size-num-1

	def row_encode(self, num):
		return self.size-num

	def __remove_cluster(self, cluster):
		for (posX,posY) in cluster:
			self.set_pos(".", posX, posY)
		return len(cluster)

	# toggle who the last player was
	def toggle_player(self):
		if self.last_player == "W":
			self.last_player = "B"
			self.this_player = "W"
		else:
			self.last_player = "W"
			self.this_player = "B"

	# handle a single move and make sure the state is accurate
	def register_move(self, move):
		self.last_raw_move = self.this_raw_move
		self.this_raw_move = move

		# ignore passes
		if move[-5:] == ":PASS":
			# toggle moves
			self.last_move = self.this_move
			self.this_move = None
			self.toggle_player()
			return

		color = move[0]
		column = move[2]
		row_no = self.row_decode(int(move[3:])-1)
		column_no = self.column_decode(column)
		self.set_pos(color, column_no, row_no)

		# count the liberties of the surrounding stones, and remove them if
		# necessary while keeping track of how many stones were killed

		killed_stones = 0

		# left stone
		if column_no-1 > 0:
			if self.board[row_no][column_no-1] != self.this_player:
				(cluster, libs) = self.count_liberties(column_no-1, row_no)
				if libs == 0:
					killed_stones = self.__remove_cluster(cluster)

		# right stone
		if column_no+1 < self.size:
			if self.board[row_no][column_no+1] != self.this_player:
				(cluster, libs) = self.count_liberties(column_no+1, row_no)
				if libs == 0:
					killed_stones = self.__remove_cluster(cluster)

		# top stone
		if row_no-1 > 0:
			if self.board[row_no-1][column_no] != self.this_player:
				(cluster, libs) = self.count_liberties(column_no, row_no-1)
				if libs == 0:
					killed_stones = self.__remove_cluster(cluster)

		# bottom stone
		if row_no+1 < self.size:
			if self.board[row_no+1][column_no] != self.this_player:
				(cluster, libs) = self.count_liberties(column_no, row_no+1)
				if libs == 0:
					killed_stones = self.__remove_cluster(cluster)

		# next move
		self.last_move = self.this_move
		self.this_move = (column_no, row_no)
		self.toggle_player()

		if tdlearner != None and self.winner != False:
			# calculate reward
			self.last_reward = self.this_reward
			self.this_reward = self.tdlearner.calculate_reward(
				self, killed_stones, column_no, row_no
			)

		if tdlearner != None and self.winner != False:
			# update board chunks
			self.tdlearner.update(self)

	# checks a given spot to see if its a new point to be added to the cluster,
	# and manages the liberty count
	def __cluster_checkspot(self, color, cluster, posX, posY, libs, new):
		spot_color = self.get_pos(posX, posY)

		# calculate liberties at this position
		if color == spot_color:
			try:
				cluster.index((posX,posY))
			except ValueError:
				cluster.append((posX,posY))
				new += 1
		elif spot_color == ".":
			libs += 1

		return (cluster,libs,new)

	def __cluster_expand(self, color, cluster, libs, new):
		# variables that are dynamic but need to stand still for the loop
		glob_new = new
		glob_cluster = cluster

		new = 0
		for i in xrange(len(glob_cluster)-glob_new, len(glob_cluster)):
			(posX,posY) = cluster[i]

			# left stone
			if posX-1 >= 0:
				(cluster,libs,new) = self.__cluster_checkspot(
					color, cluster, posX-1, posY, libs, new
				)

			# right stone
			if posX+1 < self.size:
				(cluster,libs,new) = self.__cluster_checkspot(
					color, cluster, posX+1, posY, libs, new
				)

			# top stone
			if posY-1 >= 0:
				(cluster,libs,new) = self.__cluster_checkspot(
					color, cluster, posX, posY-1, libs, new
				)

			# bottom stone
			if posY+1 < self.size:
				(cluster,libs,new) = self.__cluster_checkspot(
					color, cluster, posX, posY+1, libs, new
				)

			# recurse for more
			(cluster, libs) = self.__cluster_expand(color, cluster, libs, new)

		return (cluster, libs)

	def count_liberties(self, posX, posY):
		cluster = [(posX,posY)]
		libs = 0
		color = self.get_pos(posX,posY)

		# empty spot doesn't really have liberties, but we need to treat it as
		# if it did
		if color == ".":
			libs = 1

		# expand cluster
		else:
			(cluster, libs) = self.__cluster_expand(color, cluster, libs, 1)

		return (cluster, libs)

	def text_state(self):
		out = ""
		for i in xrange(0, self.size):
			# for current move in matching spot, loop through individually so we
			# can indicate which item was the last move
			if self.this_move != None and i == self.this_move[1]:
				row_out = ""
				for j in xrange(0, self.size):
					if self.this_move[0] == j:
						row_out += "(%s)" % self.board[i][j]
					elif self.this_move[0] == j+1:
						row_out += " %s" % self.board[i][j]
					elif self.this_move[0] == j-1:
						row_out += self.board[i][j]
					else:
						row_out += " " + self.board[i][j]

			# otherwise, just join them together with spaces
			else:
				row_out = " %s " % " ".join(self.board[i])

			# append to final output
			out += "%s\n" % row_out

		return out

	def print_tables(self):
		self.tdlearner.print_tables()
