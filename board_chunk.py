#!/bin/bash

from copy import deepcopy

RELATIVE = 0
ABSOLUTE = 1

class board_chunk(object):
	# "static" attributes
	RELATIVE = RELATIVE
	ABSOLUTE = ABSOLUTE

	# regular attributes
	game_state = None
	chunk_list = None
	chunk_size = 3
	posX = None
	posY = None
	cX = None
	cY = None

	def __init__(self, game_state):
		self.game_state = game_state

		# build blank list to start with
		self.chunk_list = []
		for i in xrange(0, self.chunk_size):
			self.chunk_list.append(["."]*3)

	def get_state(self):
		# no state to translate
		if self.chunk_list == None:
			return "........."

		# there is a state to translate
		state = ""
		for row in self.chunk_list:
			state += "".join(row)
		return state

	def __rotate_clockwise(self):
		new_chunk_list = deepcopy(self.chunk_list)
		for i in xrange(0, self.chunk_size):
			for j in xrange(0, self.chunk_size):
				new_chunk_list[i][j] = \
					self.chunk_list[self.chunk_size-j-1][i]
		self.chunk_list = new_chunk_list

	def __rotate_counterclockwise(self):
		new_chunk_list = deepcopy(self.chunk_list)
		for i in xrange(0, self.chunk_size):
			for j in xrange(0, self.chunk_size):
				new_chunk_list[i][j] = \
					self.chunk_list[j][self.chunk_size-i-1]
		self.chunk_list = new_chunk_list

	def __flip_diagonally(self):
		new_chunk_list = deepcopy(self.chunk_list)
		for i in xrange(0, self.chunk_size):
			for j in xrange(0, self.chunk_size):
				new_chunk_list[i][j] = \
					self.chunk_list\
						[self.chunk_size-j-1][self.chunk_size-i-1]
		self.chunk_list = new_chunk_list

	def __flip_across(self):
		new_chunk_list = deepcopy(self.chunk_list)
		for i in xrange(0, self.chunk_size):
			for j in xrange(0, self.chunk_size):
				new_chunk_list[i][j] = \
					self.chunk_list[i][self.chunk_size-j-1]
		self.chunk_list = new_chunk_list

	def translate(self):
		if self.has_corner():
			# no translation if we are in top left corner
			if self.cX == 1 and self.cY == 1:
				return

			else:
				# rotate clockwise for bottom left
				if self.cX == 1:
					self.__rotate_clockwise()

				# rotate counter-clockwise for top right
				elif self.cY == 1:
					self.__rotate_counterclockwise()

				# flip diagonally for bottom right
				else:
					self.__flip_diagonally()

		elif self.has_side():
			# no translation if the side is on the left
			if self.cX == 1:
				return

			else:
				# rotate counter-clockwise for top side
				if self.cY == 1:
					self.__rotate_counterclockwise()

				# rotate clockwise for bottom side
				elif self.cY == 7:
					self.__rotate_clockwise()

				# flip across for right side
				else:
					self.__flip_across()

	def untranslate(self):
		if self.has_corner():
			# no translation if we are in top left corner
			if self.cX == 1 and self.cY == 1:
				return

			else:
				# rotate clockwise for bottom left, so counter for untranslate
				if self.cX == 1:
					self.__rotate_counterclockwise()

				# rotate counter-clockwise for top right, so clockwise for
				# untranslate
				elif self.cY == 1:
					self.__rotate_clockwise()

				# flip diagonally for bottom right, so same for untranslate
				else:
					self.__flip_diagonally()

		elif self.has_side():
			# no translation if the side is on the left
			if self.cX == 1:
				return

			else:
				# rotate counter-clockwise for top side, so clockwise for
				# untranslate
				if self.cY == 1:
					self.__rotate_clockwise()

				# rotate clockwise for bottom side, so counter for untranslate
				elif self.cY == 7:
					self.__rotate_counterclockwise()

				# flip across for right side, so same for untranslate
				else:
					self.__flip_across()

	# GENERATORS {{{
	# posX, cX, posY, and cY are all absolute relative to the board

	def gen_relative(self, posX, posY):
		# store in case it's needed later
		self.posX = posX
		self.posY = posY

		# calculate center X coordinates
		if posX == 0:
			self.cX = 1
		elif posX == self.game_state.size-1:
			self.cX = self.game_state.size-2
		else:
			self.cX = posX

		# calculate center Y coordinates
		if posY == 0:
			self.cY = 1
		elif posY == self.game_state.size-1:
			self.cY = self.game_state.size-2
		else:
			self.cY = posY

		# build chunk
		for i in xrange(0, self.game_state.chunk_size_relative):
			for j in xrange(0, self.game_state.chunk_size_relative):
				self.chunk_list[j][i] = \
					self.game_state.board[self.cY-1+j][self.cX-1+i]

		self.translate()

	def gen_absolute(self, posX, posY):
		# store in case it's needed later
		self.posX = posX
		self.posY = posY

		# calculate center X and Y values
		# this relies on integer truncation of decimals
		self.cX = (self.posX/3)*3+1
		self.cY = (self.posY/3)*3+1

		# build chunk
		for i in xrange(0, self.game_state.chunk_size_absolute):
			for j in xrange(0, self.game_state.chunk_size_absolute):
				self.chunk_list[j][i] = \
					self.game_state.board[self.cY-1+j][self.cX-1+i]

		self.translate()

	# }}}

	# LOCATORS {{{
	# these methods roughly locate where on the board the chunk is, or locate
	# specific points

	def get_absolute_position(self, posX, posY):
		return (self.cX-1+posX, self.cY-1+posY)

	@staticmethod
	def is_side(posX, posY, size):
		ret = False

		if posX == 0 or posX == size-1 or posY == 0 or posY == size-1:
			ret = True

		return ret

	@staticmethod
	def is_corner(posX, posY, size):
		ret = False

		if (posX == 0 or posX == size-1) and (posY == 0 or posY == size-1):
			ret = True

		return ret

	def has_side(self):
		for i in xrange(self.cX-1, self.cX+2):
			for j in xrange(self.cY-1, self.cY+2):
				if self.is_side(i, j, self.game_state.size):
					return True

	def has_corner(self):
		for i in xrange(self.cX-1, self.cX+2):
			for j in xrange(self.cY-1, self.cY+2):
				if self.is_corner(i, j, self.game_state.size):
					return True

	# }}}
