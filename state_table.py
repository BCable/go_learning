#!/usr/bin/env python

from pprint import pprint

class state_table(object):
	table = None

	def __init__(self):
		self.table = {}

	def increment(self, state):
		if self.table.has_key(state):
			self.table[state] += 1
		else:
			self.table[state] = 1

	def get(self, state):
		if self.table.has_key(state):
			return self.table[state]
		else:
			return 0

	def recall(self, state):
		if self.table.has_key(state):
			return self.table[state]
		else:
			return -99

	def print_table(self):
		pprint(self.table)
