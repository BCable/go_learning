#!/usr/bin/env python

import re, subprocess

class go_generator(object):

	def __init__(self, watch=False):
		self.size = "9"
		# nice for debugging
		self.watch = watch

	def get_move(self, color, proc, move=None):
		# make the move
		if move != None:
			# flush output so this move doesn't get detected as the opponent's
			# move
			proc.stdout.flush()
			proc.stdin.write("%s\n" % move)

		# figure out the response
		match = False
		while not match:
			newline = proc.stdout.readline().strip()

			# i see you
			if self.watch:
				print newline

			# this regexp is in gnugo_player.py as well (in case this gets
			# changed)
			move = re.sub(
				"^%s\([0-9]+\):(?: ([A-HJ-T][0-9]+|PASS))$" % color,
				"\\1", newline
			)
			if move != newline:
				match = True

		# return response
		return move.strip()

	def send_command(self, proc, cmd):
		proc.stdin.write("%s\n" % cmd)

	def simulate_game(self):
		# log of game
		game = ""

		# proc_black is the game playing black, so this tells it that the "user"
		# wants to be white
		proc_black = subprocess.Popen(
			["gnugo",
				"--mode", "ascii",  "--boardsize", self.size, "--komi", "5.5",
				"--color", "white"
			],
			stdin=subprocess.PIPE,
			stdout=subprocess.PIPE, stderr=subprocess.PIPE
		)
		proc_white = subprocess.Popen(
			["gnugo",
				"--mode", "ascii", "--boardsize", self.size, "--komi", "5.5",
			],
			stdin=subprocess.PIPE,
			stdout=subprocess.PIPE, stderr=subprocess.PIPE
		)

		# toggle the board off, if we aren't watching it
		if not self.watch:
			self.send_command(proc_black, "showboard")
			self.send_command(proc_white, "showboard")

		# setup for main loop
		color_cur = "black"
		cur_move = None
		last_move = False # so that cur_move != last_move
		proc_cur = proc_black
		proc_last = None

		# main loop playing against each other
		# note: while it's technically not possible for the same move to occur
		# at and those moves being a pass (in Go), the if is there anyway to be
		# more generic
		while cur_move != last_move or last_move != "pass":
			last_move = cur_move
			cur_move = self.get_move(color_cur, proc_cur, last_move).lower()
			proc_last = proc_cur

			# store move
			if color_cur == "black":
				game += "B:%s\n" % cur_move.upper()
				color_cur = "white"
				proc_cur = proc_white
			else:
				game += "W:%s\n" % cur_move.upper()
				color_cur = "black"
				proc_cur = proc_black

		# now retrieve results
		#self.send_command(proc_last, "pass")
		line = proc_last.stdout.readline()
		while line != None:
			if line[0:8] == "Result: ":
				score = line[8:]
				break
			line = proc_last.stdout.readline()
			print line
		game += "RESULT:%s\n" % score

		# time to quit
		self.send_command(proc_black, "quit")
		self.send_command(proc_white, "quit")
		proc_black.wait()
		proc_white.wait()

		return game.strip()

if __name__ == "__main__":

	def zero_spacer(number, length):
		number = str(number)
		while len(number) < length:
			number = "0" + number
		return number

	gen = go_generator()
	import time
	start = time.time()
	for i in xrange(0, 5000):
		results = gen.simulate_game()
		fp = open("games/" + zero_spacer(i,4), "w")
		fp.write(results)
		fp.close()
		print zero_spacer(i,4)

	end = time.time()
	print "TOTAL TIME:", end-start
	print "AVERAGE TIME:", (end-start)/20.
