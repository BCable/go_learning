#!/usr/bin/env python

import player

class learned_player(player.player):
	next_move = None
	tdlearner = None

	def __init__(self, color, tdlearner):
		super(learned_player,self).__init__(color)
		self.tdlearner = tdlearner

	def make_move(self, game_state):
		if self.next_move == None:
			move = self.peek_move(game_state)
		else:
			move = self.next_move

		self.next_move = None
		return move

	def peek_move(self, game_state):
		if self.next_move == None:
			best_move = self.tdlearner.get_best_move(game_state, self.color)

			# pass if nothing found
			if best_move == False:
				return self.color + ":PASS"

			# return best move
			else:
				return \
					self.color + ":" + \
					game_state.column_encode(best_move[0]) + \
					str(game_state.row_encode(best_move[1]))

		else:
			return self.next_move

	def results(self, game_state):
		return False
