#!/usr/bin/env python

from state_table import state_table

class tdtables(object):
	tdlearner = None

	# state tables
	utilities = None
	frequencies = None

	# states
	last_game_state = None
	this_game_state = None

	# states as keys
	last_state = None
	this_state = None

	def __init__(self, tdlearner, game_state):
		self.tdlearner = tdlearner
		self.this_game_state = game_state

		self.utilities = state_table()
		self.frequencies = state_table()

	def update(self, game_state, board_chunk):
		# store states
		self.last_game_state = self.this_game_state
		self.this_game_state = game_state

		# get states as keys
		last_state = board_chunk.get_state()
		this_state = board_chunk.get_state()

		# update frequency table
		self.frequencies.increment(this_state)

		# update utility function
		# algorithm taken from "Artificial Intelligence: A Modern Approach"
		# second edition by Stuart Russel and Peter Norvig,
		# page 769, Figure 21.4
		if self.utilities.table.has_key(last_state):
			self.utilities.table[last_state] = \
				self.utilities.get(last_state) + (
					self.tdlearner.get_learning_rate(
						self.frequencies.get(last_state)
					) * (
						self.this_game_state.last_reward + (
							self.tdlearner.discount_factor * \
							self.utilities.get(this_state) - \
							self.utilities.get(last_state)
						)
					)
				)
		else:
			self.utilities.table[last_state] = self.this_game_state.this_reward
