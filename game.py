#!/usr/bin/env python

import re

from game_state import game_state
from tdlearner import tdlearner

from passive_player import passive_player
from learned_player import learned_player
from gnugo_player import gnugo_player

class game(object):
	game_state = None
	player_one = None
	player_two = None
	player = 1

	def __init__(self, tdlearner, player_one, player_two):
		self.player_one = player_one
		self.player_two = player_two

		# create a game state object
		self.game_state = game_state(self.results(), tdlearner)

	def at_end(self):
		at_end = False
		if (
			self.game_state != None and
			self.game_state.this_raw_move != None and
			self.game_state.last_raw_move != None
		):
			if (
				self.game_state.last_raw_move[-5:] == ":PASS" and
				self.game_state.this_raw_move[-5:] == ":PASS"
			):
				return True

		return at_end

	def step(self):
		if self.player == 1:
			next_move = self.player_one.make_move(self.game_state)
			self.player = 2
		else:
			next_move = self.player_two.make_move(self.game_state)
			self.player = 1

		self.game_state.register_move(next_move)
		return next_move

	def results(self):
		if self.player == 1:
			results = self.player_one.results(self.game_state)
			if results == False:
				results = self.player_two.results(self.game_state)
		else:
			results = self.player_two.results(self.game_state)
			if results == False:
				results = self.player_one.results(self.game_state)
		return results

# execute the learner
if __name__ == "__main__":
	def pad_zeroes(num):
		if num < 1000:
			if num < 100:
				if num < 10:
					return "000" + str(num)
				else:
					return "00" + str(num)
			else:
				return "0" + str(num)
		return str(num)

	# create the tdlearner class to pass to the game walker
	tdl = tdlearner(game_state("B"))

	for i in xrange(0, 100):
		for j in xrange(0, 5000):
			# load and store the game
			fp = open("games/" + pad_zeroes(i), "r")
			moves = fp.readlines()
			fp.close()

			# generate players
			passive_player_black = passive_player("B", moves)
			passive_player_white = passive_player("W", moves, True)

			g = game(tdl, passive_player_black, passive_player_white)
			while not g.at_end():
				move = g.step()

		print "Iterations completed:", i+1

	#g.game_state.print_tables() # DEBUG

	# tdl now has a learned knowledge base
	print "Data was successfully learned. Press enter to continue..."
	raw_input()

	for i in xrange(0, 1):
		if i%2 == 0: # learned player is black
			the_learned_player = learned_player("B", tdl)
			the_gnugo_player = gnugo_player(
				"W", g.game_state.size, g.game_state.komi
			)
			g = game(tdl, the_learned_player, the_gnugo_player)
			while not g.at_end():
				move = g.step()
				print g.game_state.text_state()

			print g.results()

		else: # learned player is white
			the_gnugo_player = gnugo_player("B")
			the_learned_player = learned_player("W", tdl)
			g = game(tdl, the_gnugo_player, the_learned_player)
