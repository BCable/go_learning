#!/usr/bin/env python

from board_chunk import board_chunk
from tdtables import tdtables

import random

class tdlearner(object):
	# chunk records
	tables_corner = None
	tables_side = None
	tables_open = None

	# reward ratios
	rr_killed_stones = 0.5
	rr_winners_move = 0.1
	rr_losers_move = -0.05
	rr_liberties = 0.9

	# discount factor
	discount_factor = 1
	learning_rate = 0.75
	learning_reduction_rate = 0.90

	def __init__(self, game_state):
		self.tables_corner = tdtables(self, game_state)
		self.tables_side = tdtables(self, game_state)
		self.tables_open = tdtables(self, game_state)

	def get_learning_rate(self, frequency):
		if frequency == 0:
			return self.learning_rate
		else:
			return self.learning_rate * (self.learning_rate**frequency)

	def calculate_reward(self, game_state, killed_stones, posX, posY):
			reward = killed_stones * self.rr_killed_stones
			reward += \
				self.rr_liberties * game_state.count_liberties(posX, posY)[1]
			if game_state.last_player == game_state.winner:
				reward += self.rr_winners_move
			else:
				reward += self.rr_losers_move

			return reward

	def update(self, game_state):
		# relative
		bc = board_chunk(game_state)
		bc.gen_relative(game_state.this_move[0], game_state.this_move[1])

		if bc.has_corner():
			self.tables_corner.update(game_state, bc)
		elif bc.has_side():
			self.tables_side.update(game_state, bc)
		else:
			self.tables_open.update(game_state, bc)

		# absolute
		bc = board_chunk(game_state)
		bc.gen_absolute(game_state.this_move[0], game_state.this_move[1])

		if bc.has_corner():
			self.tables_corner.update(game_state, bc)
		elif bc.has_side():
			self.tables_side.update(game_state, bc)
		else:
			self.tables_open.update(game_state, bc)

	def __get_chunk_best(self, game_state, color, board_chunk, tdtables):
		best_move = False
		best_score = -100
		for i in xrange(0, game_state.chunk_size_absolute):
			for j in xrange(0, game_state.chunk_size_absolute):
				if board_chunk.chunk_list[i][j] == ".":
					# get score
					board_chunk.chunk_list[i][j] = color
					score = tdtables.utilities.recall(board_chunk.get_state())
					board_chunk.chunk_list[i][j] = "."

					# get absolute position
					board_chunk.untranslate()
					(x,y) = board_chunk.get_absolute_position(j,i)

					# make sure it's blank
					if game_state.board[y][x] == ".":
						# get liberties
						game_state.board[y][x] = color
						liberties = game_state.count_liberties(x,y)[1]
						game_state.board[y][x] = "."

						# update move if necessary
						if score > best_score and liberties != 0:
							best_move = (j,i)
							best_score = score

					# fix the chunk to be oriented correctly again
					board_chunk.translate()

		return (best_move,best_score)

	def get_best_move(self, game_state, color):
		# pass if the other player does
		if (
			game_state != None and
			game_state.last_move != None and
			game_state.last_move[-4:] == "PASS"
		):
			return color + ":PASS"

		# get the scores for each move
		best_move = False
		best_score = -100

		# check each absolute chunk (9 of them, each with 9 states) for the
		# whole board, and check the relative chunk for the last move to
		# decide where the next move should be
	
		# absolute chunks
		for i in xrange(0, game_state.size/game_state.chunk_size_absolute):
			for j in xrange(0,
				game_state.size/game_state.chunk_size_absolute
			):
				move_changed = False

				# get the chunk
				bc = board_chunk(game_state)
				bc.gen_absolute(
					i*game_state.chunk_size_absolute,
					j*game_state.chunk_size_absolute
				)

				# corner chunk
				if bc.has_corner():
					(lbest_move,lbest_score) = self.__get_chunk_best(
						game_state, color, bc, self.tables_corner
					)
					if lbest_score > best_score:
						best_move = lbest_move
						best_score = lbest_score
						move_changed = True
				# side chunk
				elif bc.has_side():
					(lbest_move,lbest_score) = self.__get_chunk_best(
						game_state, color, bc, self.tables_side
					)
					if lbest_score > best_score:
						best_move = lbest_move
						best_score = lbest_score
						move_changed = True
				# open chunk
				else:
					(lbest_move,lbest_score) = self.__get_chunk_best(
						game_state, color, bc, self.tables_open
					)
					if lbest_score > best_score:
						best_move = lbest_move
						best_score = lbest_score
						move_changed = True

				if move_changed:
					# translate move to be back in the real position rather than
					# relative to the translation
					bc.untranslate()
					best_move = bc.get_absolute_position(
						best_move[0], best_move[1]
					)

					# if the move is an illegal move, just choose a random move
					# with liberties
					while game_state.board[best_move[1]][best_move[0]] != ".":
						rx = random.randrange(0, game_state.size)
						ry = random.randrange(0, game_state.size)

						if game_state.board[ry][rx] == ".":
							game_state.board[ry][rx] = color
							libs = game_state.count_liberties(rx,ry)[1]
							game_state.board[ry][rx] = "."
							if libs != 0:
								best_move = (rx,ry)
								best_score = -100

		# relative chunk
		if game_state.this_move != None:
			(x,y) = game_state.this_move
			bc = board_chunk(game_state)
			bc.gen_relative(x,y)

			# corner chunk
			if bc.has_corner():
				(lbest_move,lbest_score) = self.__get_chunk_best(
					game_state, color, bc, self.tables_corner
				)
			# side chunk
			elif bc.has_side():
				(lbest_move,lbest_score) = self.__get_chunk_best(
					game_state, color, bc, self.tables_side
				)
			# open chunk
			else:
				(lbest_move,lbest_score) = self.__get_chunk_best(
					game_state, color, bc, self.tables_open
				)

			# update if necessary
			if lbest_score > best_score:
				best_score = lbest_score
				best_move = lbest_move

				bc.untranslate()
				best_move = bc.get_absolute_position(
					best_move[0], best_move[1]
				)

		return best_move

	def print_tables(self):
		print
		print "CORNER:"
		self.tables_corner.frequencies.print_table()
		self.tables_corner.utilities.print_table()

		print
		print "SIDE:"
		self.tables_side.frequencies.print_table()
		self.tables_side.utilities.print_table()

		print
		print "OPEN:"
		self.tables_open.frequencies.print_table()
		self.tables_open.utilities.print_table()
